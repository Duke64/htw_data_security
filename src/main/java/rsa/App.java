package rsa;

import java.math.BigInteger;

/**
 * Description
 *
 * @author Tom Dittrich s0555944@htw-berlin.de
 * @version 0.1
 */
public class App {
    public static void main(String[] args) {
        KeyGenerator keyGenerator = new KeyGenerator();

        keyGenerator.generateNumbers();
        keyGenerator.printNumbers();

        BigInteger message = new BigInteger("");
        System.out.println("Message: " + message);

        BigInteger encryptedMessage = EncryptMessage.encrypt(keyGenerator.n, keyGenerator.e, message);
        System.out.println("Encrypted: " + encryptedMessage);

        BigInteger decryptedMessage = DecryptMessage.deencrypt(keyGenerator.n, keyGenerator.d, encryptedMessage);
        System.out.println("Decrypt: " + decryptedMessage);
    }
}
