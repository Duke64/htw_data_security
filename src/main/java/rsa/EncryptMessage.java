package rsa;

import java.math.BigInteger;

/**
 * Description
 *
 * @author Tom Dittrich s0555944@htw-berlin.de
 * @version 0.1
 */
public class EncryptMessage {

    public static BigInteger encrypt(BigInteger n, BigInteger e, BigInteger message){
        return message.modPow(e, n);
    }
}
