package rsa;

import java.math.BigInteger;
import java.util.Random;

/**
 * Original source code:
 * http://www.herongyang.com/Cryptography/RSA-BigInteger-RsaKeyGenerator-java.html
 *
 * @author Tom Dittrich s0555944@htw-berlin.de
 * @version 0.1
 */
public class KeyGenerator {
    int size = 500;
    BigInteger p;
    BigInteger q;
    BigInteger n;
    BigInteger m;
    BigInteger e;
    BigInteger d;

    public void generateNumbers(){
        Random rnd = new Random();
        p = BigInteger.probablePrime(size / 2, rnd);
        q = p.nextProbablePrime();
        n = p.multiply(q);

        m = (p.subtract(BigInteger.ONE)).multiply(
                q.subtract(BigInteger.ONE));
        e = getCoprime(m);
        d = e.modInverse(m);
    }

    public void printNumbers() {
        System.out.println("n: " + n);
        System.out.println("Public key: " + e);
        System.out.println("Private key: " + d);
    }

    public static BigInteger getCoprime(BigInteger m) {
        Random rnd = new Random();
        int length = m.bitLength() - 1;
        BigInteger e = BigInteger.probablePrime(length, rnd);
        while (!(m.gcd(e)).equals(BigInteger.ONE)) {
            e = BigInteger.probablePrime(length, rnd);
        }
        return e;
    }
}
